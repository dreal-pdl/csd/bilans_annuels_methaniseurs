---
title: "Exploitation réponses intrants redressées - campagne `r params$campagne`"
author: "Juliette Engelaere-Lefebvre"
date: "`r format(Sys.Date(), '%d/%m/%Y')`"
output: 
  html_document:
    df_print: paged
    toc: TRUE
    toc_depth: 4
    toc_float: TRUE
    keep_md: FALSE
params:
  vpn: TRUE
  campagne: "2023"
  sgbd_util: "does"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(COGiter)
library(readODS)
library(readxl)
library(tricky)
library(lubridate)
library(datalibaba)

# Chargement des réponses assemblées et des données géo consolidées par TEO
load(paste0("1-exports_", params$campagne, ".RData"))
load(paste0("0-donnees_ref", params$campagne, ".RData"))
# menage dans l'environnement de travail : on supprime les palettes, les éléments pour les visualisation des intrants et les fonctions de formatage des tableaux
rm(list = ls(pattern = "^pal_|^vec_col|_tot$|^palette_"))
rm(famille_ordre, filigrane)
```

# Campagnes précédentes


## Lecture intrants redressés campagne précédentes (2019 à `r as.numeric(params$campagne) - 1`)
```{r}
tables_intrants <- list_tables(schema = "mecc_bilans_metha", db = "production", user = params$sgbd_util) %>% 
  enframe %>% 
  filter(grepl("intrants", value), grepl("redresses", value)) %>% 
  mutate(campagne = gsub("intrants_|_redresses", "", value)) %>% 
  filter(params$campagne > campagne) %>% 
  pull(value)

intrants_prev <- map_dfr(.x = tables_intrants,
                         .f = ~importer_data(table = .x, schema = "mecc_bilans_metha", db = "production", user = params$sgbd_util) %>% 
                           mutate(origine = .x)) 
```



## Campagne actuelle

### Tables d'aide aux redressement des instrants
TODO Détecter les anormalement basses ou hautes par rapport au millésimes précédents
TODO Abaisser le seuil de détection des déclarations anormales à un facteur 100 par rapport aux volumes de projet en tenant compte du nombre de mois d'exercices.

> TODO 2022 origine des intrants :   

>* vérifier que le tonnage provenant de + de 50 km ("dont_tonnage_plus_50km") < tonnage_intrant_exterieur.
>* `dist_appro_km_max` >= 50 km si `dont_tonnage_plus_50km` renseigné
>* `dist_appro_km_max` renseigné si `tonnage_intrant_exterieur` renseigné.

```{r}
intrants_bruts <- intrants_compil3

# corrections des erreurs d'unites : liste des unités anormalement haute ou basse par rapport aux volume intrant de projet
comp_projet <- compil_reponses5 %>% 
  select(id_aile, entreprise_raison_sociale, date_mise_service = Date_MeS, tonnage_projet_aile, tonnage_total) %>% 
  mutate(prctage_declar_projet = (tonnage_total/tonnage_projet_aile) %>% round(4) * 100,
         anomalie = case_when(
           tonnage_projet_aile != 0 & prctage_declar_projet > 500000 ~ "declaration intrants 1000 fois trop elevee",
           tonnage_projet_aile != 0 & prctage_declar_projet < 0.13 ~ "declaration intrants 1000 fois trop faible",
           TRUE ~ "")) 
decl_1000_fois_trop_fortes <- filter(comp_projet, anomalie == "declaration intrants 1000 fois trop elevee") %>% pull(id_aile)
decl_1000_fois_trop_faibles <- filter(comp_projet, anomalie == "declaration intrants 1000 fois trop faible") %>% pull(id_aile)

evolutions_anormales <- intrants_prev %>% 
  group_by(campagne, id_aile, date_mise_service) %>% 
  summarise(tonnage_total = sum(tonnage_total), .groups = "drop") %>% 
  mutate(nb_jours_act = difftime(ymd(paste0(campagne, "-12-31")), date_mise_service, units = "days"),
         prod_annuel_representative = nb_jours_act > 350,
         nb_jours_act_campagne = if_else(prod_annuel_representative, 365, as.numeric(nb_jours_act)),
         nb_mois_act = if_else(prod_annuel_representative, 12, as.numeric(nb_jours_act)/30) %>% round(0),
         tonnage_quot = (tonnage_total / nb_jours_act_campagne) %>% round(2)) %>% 
  select(-date_mise_service) %>% 
  pivot_wider(id_cols = c(id_aile), names_from = campagne, values_from = c(tonnage_quot, tonnage_total), names_glue = "{.value}_{campagne}") %>% 
  left_join(select(compil_reponses5, id_aile, tonnage_total) %>% 
              mutate(tonnage_total = (tonnage_total / 365) %>% round(2)) %>% # nota le left_join ecarte les ets mis en service dans l'annee
              rename_with(~gsub("tonnage_total", paste0("tonnage_quot_", params$campagne), .x)),
            by = "id_aile") 

aide_redressement <- comp_projet %>% left_join(evolutions_anormales, by = "id_aile") %>% 
  rowwise() %>% 
  mutate(ecarts_interannuel_pct = sd(c(tonnage_quot_2022, tonnage_quot_2021, tonnage_quot_2020, tonnage_quot_2019), na.rm = TRUE) /
           mean(c(tonnage_quot_2020, tonnage_quot_2019), na.rm = TRUE) * 100,
         ecarts_interannuel_pct = round(ecarts_interannuel_pct, 2)) %>% 
  select(id_aile, entreprise_raison_sociale, date_mise_service, tonnage_projet_aile, prctage_declar_projet, 
         tonnage_total_2022 = tonnage_total, contains("tonnage_total"), contains("tonnage_quot_"),
         # tonnage_quot_2019, tonnage_quot_2020, tonnage_quot_2021, tonnage_quot_2022, 
         ecarts_interannuel_pct, anomalie)
```

### Redressements des intrants

```{r}
# fonction de détection d'une chaine de texte dans les précisions
vu_ds_precision <- function(data, motif_text = "sorgho", col_1 = "precision_intrant_exterieur", 
                            col_2 = "precision_intrant_prod_en_propre") {
  (str_detect(tolower(data[[col_1]]), motif_text) | str_detect(tolower(data[[col_2]]), motif_text))
}

# corrections effectuées à partir du fichier "donnees_redresses/20240228 HKNB indicateurs intrants 2022 corSTEP4453_W.xlsx"

intrants_redresses <- intrants_bruts %>% 
  filter(typo_aile != "ISDND") %>% # pas d'intrants pour les ISDND
  select(-Famille_intrants) %>% 
  mutate(cat_intrant2 = case_when(
    vu_ds_precision(., "sorgho") & grepl("hiver", cat_intrant) ~ "CIVE d’été (préciser la nature)",
    vu_ds_precision(., "ray grass") & grepl("été", cat_intrant) ~ "CIVE d’hiver (préciser la nature)",
    vu_ds_precision(., "seigle") & grepl("été", cat_intrant) ~ "CIVE d’hiver (préciser la nature)",
    vu_ds_precision(., "issu.* de silo") & !(cat_intrant %in% c("maïs", "herbe", "colza")) ~ 
      "Déchets de stockage (résidu de silo, de séchage..)",
    vu_ds_precision(., "^colza$") ~ "colza",
    vu_ds_precision(., "lactoserum") ~ "IAA du lait",
    vu_ds_precision(., "pomme a couteaux") ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    vu_ds_precision(., "cuisson gateaux à base d'huiles végétale") ~ "IAA des huiles et matières grasses (sans produits animaux)",
    vu_ds_precision(., "pain de mie") ~ "IAA meunerie et amidonerie",
    vu_ds_precision(., "marc de rais.n ") ~ "IAA des fruits",
    vu_ds_precision(., "déchets de camomille et de plantes médicinales") ~ "Autres résidus – préciser la nature",
    vu_ds_precision(., "fauche de bord de r") ~ "Fauches de bords de route", 
    vu_ds_precision(., "fauche de bords de r") ~ "Fauches de bords de route", 
    vu_ds_precision(., "^tonte de pelouse$") & str_detect(cat_intrant, 'Autres résidus') ~ 
      "Déchets verts des ménages ou collectivités (dont déchets verts de déchetteries)",
    vu_ds_precision(., "déchet huile de colza") ~ "IAA des huiles et matières grasses (sans produits animaux)",
    vu_ds_precision(., "refus") & cat_intrant == "maïs" ~ "Autres ensilages (préciser la nature)", 
    vu_ds_precision(., "^méthaplus$") ~ "IAA des aliments du bétail",
    id_DS == '12380688' & cat_intrant == 'Autres biodéchets issus des ménages et collectivités - préciser la nature' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '12052677' & cat_intrant == 'IAA des huiles et matières grasses (sans produits animaux)' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '12085796' & cat_intrant == 'IAA des huiles et matières grasses (sans produits animaux)' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '12041376' & cat_intrant == 'IAA des huiles et matières grasses (sans produits animaux)' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '12002127' & cat_intrant == 'IAA meunerie et amidonerie' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '11857279' & cat_intrant == 'IAA meunerie et amidonerie' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '11767807' & cat_intrant == 'IAA meunerie et amidonerie' ~ "Autres déchets végétaux venant d'IAA – préciser la nature",
    id_DS == '11783532' & cat_intrant == 'Autres ensilages (préciser la nature)' ~ "Autres ensilages (préciser la nature)",
    id_DS == '12085807' & cat_intrant == 'colza' ~ "Autres ensilages (préciser la nature)",
    id_DS == '11793393' & cat_intrant == 'Déchets de céréales et oléoprotéagineux (séchage, stockage, tri…)' ~ "Autres résidus – préciser la nature",
    id_DS == '11803324' & cat_intrant == 'Déchets de stockage (résidu de silo, de séchage..)' ~ "Autres résidus – préciser la nature",
    id_DS == '12070608' & cat_intrant == 'IAA des légumes' ~ "Biodéchets en vrac issus de collecteurs privés",
    id_DS == '12086904' & cat_intrant == 'Autres résidus – préciser la nature' ~ "Déchets de stockage (résidu de silo, de séchage..)",
    id_DS == '12085807' & cat_intrant == 'Autres résidus – préciser la nature' ~ "Déchets de stockage (résidu de silo, de séchage..)",
    id_DS == '12101793' & grepl("^Résidus de cultures céréalières", cat_intrant) ~ "Déchets de stockage (résidu de silo, de séchage..)",
    id_DS == '12294162' & grepl("^Résidus de cultures céréalières", cat_intrant) ~ "Déchets de stockage (résidu de silo, de séchage..)",
    id_DS == '12057226' & cat_intrant == 'Fumier volaille' ~ "Fumiers bovins",
    id_DS == '11919877' & cat_intrant == "Autres déchets végétaux venant d'IAA – préciser la nature" ~ "IAA des fruits",
    id_DS == '11842255' & cat_intrant == 'Autres – préciser la nature' ~ "IAA des huiles et matières grasses (sans produits animaux)",
    id_DS == '12041376' & cat_intrant == 'IAA des aliments du bétail' ~ "IAA du lait",
    id_DS == '11793393' & cat_intrant == "Autres déchets végétaux venant d'IAA – préciser la nature" ~ "IAA du sucre",
    id_DS == '11913150' & cat_intrant == 'Déchets de stockage (résidu de silo, de séchage..)' ~ "Jus de silo d’ensilage et jus de fumière d’un élevage",
    id_DS == '12111559' & cat_intrant == 'Jus de silo d’ensilage et jus de fumière d’un élevage' ~ "Lisiers bovins",
    id_DS == '11857279' & cat_intrant == 'Lisiers autres (équin, caprin, ovin… préciser)' ~ "Lisiers bovins",
    id_DS == '11873841' & cat_intrant == "Autres déchets végétaux venant d'IAA – préciser la nature" ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre",
    id_DS == '11970294' & cat_intrant == 'Autres ensilages (préciser la nature)' ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre",
    id_DS == '12074332' & cat_intrant == 'Autres résidus – préciser la nature' ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre",
    id_DS == '12055552' & vu_ds_precision(., "déchets céréales") ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre",
    id_DS == '12085913' & vu_ds_precision(., "ma.s doux") ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre",
    id_DS == '12126391' & cat_intrant == "Résidus de fruits ou légumes issus d'exploitation maraîchère Préciser la nature: feuille et fanes, légume mal calibré…" ~ "Résidus de cultures céréalières\nPréciser la nature: paille, menu paille, canne de maïs, autre", 
    id_DS == '11869130' & cat_intrant == "Autres déchets végétaux venant d'IAA – préciser la nature" ~ "Résidus de fruits ou légumes issus d'exploitation maraîchère Préciser la nature: feuille et fanes, légume mal calibré…", 
    id_DS == '12085913' & vu_ds_precision(., "pomme de terre") ~ "Résidus de fruits ou légumes issus d'exploitation maraîchère Préciser la nature: feuille et fanes, légume mal calibré…", 
    id_DS == '11793393' & cat_intrant == 'IAA des légumes' ~ "Résidus de fruits ou légumes issus d'exploitation maraîchère Préciser la nature: feuille et fanes, légume mal calibré…", 
    TRUE ~ cat_intrant),
    tonnage_intrant_prod_en_propre = case_when(
      id_DS == '11975393' & tonnage_intrant_prod_en_propre == 288688 ~ 49395,
      id_DS == '12283554' & tonnage_intrant_prod_en_propre == 7095 ~ 64920,
      id_aile %in% decl_1000_fois_trop_fortes ~ tonnage_intrant_prod_en_propre/1000,
      id_aile %in% decl_1000_fois_trop_faibles ~ tonnage_intrant_prod_en_propre*1000,
      TRUE ~ tonnage_intrant_prod_en_propre),
    tonnage_intrant_exterieur = case_when(
      id_aile %in% decl_1000_fois_trop_fortes ~ tonnage_intrant_exterieur/1000,
      id_aile %in% decl_1000_fois_trop_faibles ~ tonnage_intrant_exterieur*1000,
      TRUE ~ tonnage_intrant_exterieur),
    marqueur_redressmt = case_when(
      id_DS %in% c('11975393', '12283554') ~ "Tonnage des STEP",
      cat_intrant2 != cat_intrant ~ "Pb Famille et catégorie",
      id_aile %in% decl_1000_fois_trop_fortes ~ "Pb unites tonnage / 1000",
      id_aile %in% decl_1000_fois_trop_faibles ~  "Pb unites tonnage * 1000",
      TRUE ~ "Aucun") %>% as.factor(),
    tonnage_total = coalesce(tonnage_intrant_exterieur, 0) + coalesce(tonnage_intrant_prod_en_propre, 0),
    cat_intrant = cat_intrant2,
  ) %>% 
  select(-cat_intrant2) %>% 
  left_join(typo_dechets %>% filter(num_ordre_cat != 58.9999) %>% select(Famille_intrants, cat_intrant = Categorie_intrants), 
            by = "cat_intrant") %>% 
  arrange(Dep, as.numeric(id_aile), Famille_intrants, cat_intrant, tonnage_intrant_prod_en_propre, tonnage_intrant_exterieur) 

# verif jointure ref intrant
intrants_redresses %>% filter(is.na(Famille_intrants)) %>% arrange(cat_intrant) 


save.image(paste0("2-intrants_redr_", params$campagne, ".RData"))

```
### Autres redressements

Puissance électrique
```{r}
compil_reponses_sans_RGPD <- compil_reponses_sans_RGPD %>% 
  mutate(P_installee_kWe = if_else(P_installee_kWe > 100000, P_installee_kWe / 1000, P_installee_kWe))

```


# Incorporation des redressements dans l'export toutes réponses, enrichissement et autres exports

## données individuelles redressées des intrants : stockage SGBD
```{r exports SGBD, eval=params$sgbd_util=="does"}

poster_data(data = intrants_redresses %>% mutate(campagne = params$campagne),
            table = paste0("intrants_", params$campagne, "_redresses"), schema = "mecc_bilans_metha", db = "production",
            post_row_name = TRUE, overwrite = TRUE, droits_schema = TRUE)

```


## Compilation pluriannuelle des déclarations intrants (depuis 2019)		

```{r}
compil_pluriannuelle_intrants <- intrants_redresses %>% 
  mutate(campagne = params$campagne) %>% 
  bind_rows(intrants_prev %>% 
              mutate(cat_intrant = gsub("^Boues de station d'épuration urbaine$", 
                                        "Boues de station d'épuration urbaine - en tonnes matière sèche", cat_intrant))
         )

# Les millésimes d'enquêtes disponibles
mil_disp <- unique(compil_pluriannuelle_intrants$campagne) %>% sort(decreasing = TRUE)

```

## calcul des agrégats par installation

tonnage_total_rev
% Effluents : Effluents d'élevage : tonnage total
% Cultures principales 
% CIVE dans la ration : famille Ensilage de cultures intermédiaires / tonnage total
% Résidus vegetaux provenant des exploitations agricoles
% déchets verts
% déchets vgtx IAA
% déchets animaux Abattoirs
% déchets animaux IAA
% dechets STEP urbaine
% Biodéchets
% Glycerine
 revient au % de chaque famille (à renommer) dans la ration
 
```{r indicateurs par installation}


indic_installation_mil <- intrants_redresses %>% 
  left_join(typo_dechets, by = c("cat_intrant" = "Categorie_intrants", "Famille_intrants")) %>% 
  mutate(famille_intrants_court = str_standardize(famille_intrants_court)) %>% 
  group_by(id_DS, id_aile, famille_intrants_court) %>% 
  summarise(tonnage_total = sum(tonnage_total, na.rm = TRUE), .groups = "drop_last") %>% 
  mutate(ration_t = sum(tonnage_total)) %>% 
  arrange(famille_intrants_court) %>% 
  pivot_wider(id_cols = c(id_DS, id_aile, ration_t), names_from = famille_intrants_court, values_from = tonnage_total, values_fill = 0) %>% 
  # attention à l'ordre des famille pour que la selection ci dessous prenne tout le monde
  mutate(across(c(biodechets:residus_vegetaux_des_exploitations_agricoles), list(tonnes = ~.x, part = ~(.x / ration_t) %>% round(5)))) %>% 
  select(-c(biodechets:residus_vegetaux_des_exploitations_agricoles)) %>% 
  ungroup() %>% 
  left_join(
    intrants_redresses %>% group_by(id_DS) %>% 
      summarise(
        tonnage_intrant_prod_en_propre = sum(tonnage_intrant_prod_en_propre, na.rm = TRUE), 
        tonnage_intrant_exterieur = sum(tonnage_intrant_exterieur, na.rm = TRUE), .groups = "drop"
      )
  )

indic_installation_prec <- compil_pluriannuelle_intrants %>% 
  filter(campagne != params$campagne) %>% 
  left_join(typo_dechets, by = c("cat_intrant" = "Categorie_intrants", "Famille_intrants")) %>% 
  mutate(famille_intrants_court = str_standardize(famille_intrants_court)) %>% 
  group_by(id_aile, campagne, famille_intrants_court) %>% 
  summarise(tonnage_total = sum(tonnage_total, na.rm = TRUE), .groups = "drop_last") %>% 
  mutate(ration_t = sum(tonnage_total)) %>% 
  arrange(famille_intrants_court) %>% 
  pivot_wider(id_cols = c(id_aile, ration_t, campagne), names_from = famille_intrants_court, values_from = tonnage_total, values_fill = 0) %>% 
  # attention à l'ordre des famille pour que la selection ci dessous prenne tout le monde
  mutate(across(c(biodechets:residus_vegetaux_des_exploitations_agricoles), list(tonnes = ~.x, part = ~(.x / ration_t) %>% round(5)))) %>% 
  select(id_aile, campagne, ration_t, cive_part, cultures_principales_part) %>% 
  pivot_wider(id_cols = id_aile, names_from = campagne, values_from = c(ration_t, cive_part, cultures_principales_part), 
              names_glue = "{.value}_{campagne}")
  


indic_installation_0 <- left_join(indic_installation_mil, indic_installation_prec, by = "id_aile") %>% 
  select(-id_aile)



```
 
 

## maj onglet toutes réponses

### onglet de compléments

```{r incorporation compil toute réponses}

compil_reponses5 <- compil_reponses_sans_RGPD %>% 
  select(Dep, id_aile, s3ic, id_DS, typo_aile, entreprise_raison_sociale, Date_MeS, 
         mwh_ep_projet_aile, tonnage_projet_aile) %>% 
  left_join(indic_installation_0) %>% 
  select(Dep, id_aile, entreprise_raison_sociale, s3ic, id_DS, typo_aile, Date_MeS, contains('projet'), 
         ration_t, contains("tonnage"), everything(), -contains("_tonnes")) %>% 
  mutate(across(contains('projet'), as.numeric)) 

compil_reponses5

```

### Onglet toutes réponses

```{r}
compil_reponses_ssRGPD_redr <- compil_reponses_sans_RGPD %>% 
  select(-tonnage_intrant_prod_en_propre, -tonnage_intrant_exterieur, -tonnage_total) %>% 
  left_join(select(compil_reponses5, id_DS, ration_t, tonnage_intrant_prod_en_propre, tonnage_intrant_exterieur), by = join_by(id_DS))

poster_data(data = compil_reponses_ssRGPD_redr,
            table = paste0("reponses_principales_", params$campagne, "_redressees"), schema = "mecc_bilans_metha", 
            db = "production",
            post_row_name = FALSE, pk = "id_DS", overwrite = TRUE, droits_schema = TRUE)
```

# Demandes Nathalie suivi SRB

## Agrégation des familles d'intrants selon nomenclature SRB et Solagro

### Calculs agrégats SRB

```{r indic group SRB}

indic_suivi_SRB <- typo_dechets %>% 
  select(Famille_intrants, famille_srb) %>% 
  distinct() %>% 
  full_join(obj_2030, by = "famille_srb") %>% 
  right_join(compil_pluriannuelle_intrants, by = "Famille_intrants") %>% 
  select(campagne, contains("srb"), obj_2030_tonnes, tonnage_total, gisement_total) %>% 
  group_by(campagne, famille_srb, lib_fam_srb, obj_2030_tonnes, gisement_total) %>% 
  summarise(tonnages_declares = sum(tonnage_total, na.rm = TRUE), .groups = "drop") %>% 
  bind_rows(
    # on écarte les cultures principales pour calculer le total
    filter(., famille_srb != "Z") %>% 
      group_by(campagne) %>% 
      summarise(famille_srb = "Y", lib_fam_srb = "Total SRB", .groups = "drop", 
                across(.cols = c(tonnages_declares, obj_2030_tonnes, gisement_total), .fns = ~sum(.x, na.rm = TRUE)))
  ) %>% 
  mutate(avancement_objectif = tonnages_declares / obj_2030_tonnes,
         mobilisation_gisement = tonnages_declares / gisement_total) %>% 
  pivot_wider(id_cols = c(famille_srb, lib_fam_srb, gisement_total, obj_2030_tonnes), names_from = campagne, 
              values_from = c(tonnages_declares, avancement_objectif, mobilisation_gisement)) %>% 
  arrange(famille_srb)
              
indic_suivi_SRB

```

### Calculs agrégats solagro

```{r solagro}

gsmt_sol_dep_mil <- expand_grid(Dep = ter, campagne  = mil_disp) %>% 
  cross_join(gismts_solagro) 

indic_suivi_solagro_base <- typo_dechets %>% 
  select(cat_intrant = Categorie_intrants, Famille_intrants, type_gismnt_solagro) %>% 
  distinct() %>% 
  full_join(gsmt_sol_dep_mil, by = "type_gismnt_solagro", relationship = "many-to-many") %>% 
  full_join(compil_pluriannuelle_intrants, .,  by = c("cat_intrant", "Famille_intrants", "Dep", "campagne")) %>% 
  select(campagne, Dep, contains("solagro"), names(gismts_solagro), tonnage_total, gismt_reg = tMB_gismt_solagro) %>% 
  group_by(campagne, ordre_solagro, type_gismnt_solagro, obj_srb_solagro, Dep, gismt_reg, gsmt_44, gsmt_49, gsmt_53, gsmt_72, gsmt_85) %>% 
  summarise(tonnages_declares = sum(tonnage_total, na.rm = TRUE), .groups = "drop") %>% 
  pivot_longer(cols = c(gsmt_44, gsmt_49, gsmt_53, gsmt_72, gsmt_85, gismt_reg), 
               names_to = c(NA, "zone"), values_to = "gsmt_tmb", names_sep = "_") %>%
  filter(Dep == zone | zone == "reg") %>% 
  select(-Dep) %>% 
  group_by(campagne, ordre_solagro, type_gismnt_solagro, obj_srb_solagro, zone, gsmt_tmb) %>% 
  summarise(tonnages_declares = sum(tonnages_declares, na.rm = TRUE), .groups = "drop") %>% 
  bind_rows(
    # on écarte les cultures principales pour calculer le total
    filter(.,  type_gismnt_solagro != "Hors gisements étude TEO Solagro") %>% 
      group_by(campagne, zone) %>% 
      summarise(ordre_solagro = 8.5, type_gismnt_solagro = "Total périmètre étude Solagro TEO", .groups = "drop", 
                across(.cols = c(obj_srb_solagro, tonnages_declares, gsmt_tmb), .fns = ~sum(.x, na.rm = TRUE)))
  ) %>% 
  arrange(ordre_solagro) %>% 
  mutate(type_gismnt_solagro = fct_inorder(type_gismnt_solagro), 
         tx_mob_gsmt = round((tonnages_declares / gsmt_tmb * 100), 3))

indic_suivi_solagro_xls_0 <- indic_suivi_solagro_base %>% 
  pivot_wider(id_cols = c(zone, type_gismnt_solagro, obj_srb_solagro, gsmt_tmb), names_from = c(campagne), 
              values_from = c(tonnages_declares, tx_mob_gsmt)) %>% 
  arrange(zone, type_gismnt_solagro)
 
indic_suivi_solagro_xls <-  bind_rows(
  filter(indic_suivi_solagro_xls_0, zone == "reg"),
  filter(indic_suivi_solagro_xls_0, zone != "reg") %>% mutate(obj_srb_solagro = NA)
)
rm(indic_suivi_solagro_xls_0)
indic_suivi_solagro_xls
```


## Tableau de suivi fin par catégorie d'intrants

```{r}

df <- compil_pluriannuelle_intrants %>% 
  select(campagne, Dep, id_aile, Famille_intrants, cat_intrant, tonnage_total) %>% 
  left_join(typo_dechets, by = c("Famille_intrants", "cat_intrant" = "Categorie_intrants"))

lignes_cat <- df %>% 
  group_by(campagne, num_ordre_cat, Famille_intrants, cat_intrant, Dep) %>% 
  summarise(tonnages_declares = sum(tonnage_total, na.rm = TRUE), .groups = "drop") %>% 
  bind_rows(
    group_by(., campagne, num_ordre_cat, Famille_intrants, cat_intrant) %>% 
      summarise(Dep = "0-PdL", tonnages_declares = sum(tonnages_declares), .groups = "drop")
  ) 

lignes_fam <- lignes_cat %>% 
  group_by(campagne, Famille_intrants, Dep) %>% 
  summarise(tonnages_declares = sum(tonnages_declares), .groups = "drop") %>% 
  left_join(typo_dechets %>% group_by(Famille_intrants, famille_intrants_court) %>% summarise(num_ordre_fam = min(num_ordre_cat), .groups = "drop" ), 
            by = "Famille_intrants") %>% 
  bind_rows(group_by(., campagne, Dep) %>% 
              summarise(Famille_intrants = "Total", famille_intrants_court = "Total", num_ordre_fam = 999, 
                        tonnages_declares = sum(tonnages_declares), .groups = "drop")) %>% 
  arrange(campagne, Dep, num_ordre_fam) %>% 
  group_by(campagne, Dep) %>% 
  mutate(cat_intrant = toupper(famille_intrants_court),
         num_ordre_cat = num_ordre_fam-0.3,
         v_pourcentage = tonnages_declares / max(tonnages_declares)) %>% 
  select(-Famille_intrants, -num_ordre_fam, famille_intrants_court)
  
  

suivi_fin <- df %>% 
  group_by(campagne, Dep) %>% 
  summarise(tonnages_declares = n_distinct(id_aile), num_ordre_cat = 0, 
            cat_intrant = "Echantillon : nombre d’installations déclarantes", .groups = "drop") %>% 
  bind_rows(
    group_by(., campagne) %>% 
      summarise(tonnages_declares = sum(tonnages_declares), num_ordre_cat = 0, Dep = "0-PdL",
                cat_intrant = "Echantillon : nombre d’installations déclarantes", .groups = "drop")
  ) %>% 
  bind_rows(lignes_cat %>% select(-Famille_intrants)) %>% 
  bind_rows(lignes_fam) %>% 
  arrange(Dep, campagne, num_ordre_cat) %>% 
  relocate(tonnages_declares, .before = v_pourcentage) %>% 
  pivot_wider(id_cols = c(num_ordre_cat, cat_intrant), names_from = c(Dep, campagne), values_from = c(tonnages_declares, v_pourcentage), 
              names_glue = "{Dep}_{campagne}_{.value}", names_sort = TRUE) %>% 
  arrange(num_ordre_cat) 

agencement <- suivi_fin %>% 
  select(-contains("cat")) %>% 
  names() %>% 
  sort()


suivi_fin <- suivi_fin %>% 
  select(contains("cat"), all_of(agencement))


```

## Export xls

```{r}
writexl::write_xlsx(x = list(compil_reponses_ss_RGPD_redresse = compil_reponses_ssRGPD_redr,
                             intrants_redresses = intrants_redresses,
                             reponse_complements_intrant = compil_reponses5,
                             comp_projet_annees_prcdtes = aide_redressement,
                             Suivi_intrants =  suivi_fin,
                             Suivi_SRB = indic_suivi_SRB,
                             Suivi_SOLAGRO = indic_suivi_solagro_xls), 
                    col_names = TRUE,
                    path = paste0(getwd(), "/compilation/", lubridate::today(), "_indicateurs_intrants_", params$campagne, ".xlsx"))
                
    
save(compil_reponses5, compil_reponses_ssRGPD_redr, compil_pluriannuelle_intrants, mil_disp,
     indic_suivi_SRB, indic_suivi_solagro_base, suivi_fin, 
     file = paste0("2-intrants_pour_valo_", params$campagne,".RData"))


```


```{r, eval=FALSE}
rmarkdown::render(input = "2_redressement_reponses.Rmd", 
                  output_file = paste0("compilation/", Sys.Date(), "-2_redressement_reponses_", params$campagne, ".html"))
```

