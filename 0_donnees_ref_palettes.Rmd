---
title: "Données de références et palettes bilans annuels méthaniseurs `r params$campagne`"
author: "Juliette Engelaere-Lefebvre"
date: "`r format(Sys.Date(), '%d/%m/%Y')`"
output: html_document
params:
  vpn: TRUE
  campagne: "2023"
  sgbd_util: "does"
  compil_teo: "compilation_metha_teo_20230310"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(tidyverse)
library(readODS)
library(readxl)
library(tricky)
library(lubridate)
library(datalibaba)
library(COGiter)
library(sf)
library(glue)
library(gouvdown)
```

Les données de références sont lues à différents endroits :  

- lors de la compil TEO
- lors de la lecture des données
- lors du redressement des données  


Egalement les palettes, sont créées à différents moments, cela rend compliqué l'appropriation du projet par d'autres.

Il est nécessaire de factoriser tout cela pour dans un Rmd à lire avant la compilation des réponses pour plus de clarté.  

# Paramètres

  - [x] filigrane (intro 4e rmd)
  - [x] adresse fichier données complémentaire
  - [x] libellés régional et départementaux

```{r parametres}
# filigrane nom source
filigrane <- paste0("Bilans de fonctionnement biogaz ", params$campagne, " - DREAL Pays de la Loire")

if(params$vpn) {
  repertoire <- paste0("../W_exports_DS/", params$campagne, "/")
} else {
  repertoire <- paste0("W:/SRB/Bilans annuels méthanisation ", params$campagne, "/") 
}

fic_ods <- list.files(repertoire, full.names = TRUE) %>% grep(".ods|.xls", ., value = TRUE) %>% 
  setdiff(grep("_exemple|_suivi|modifVisi", ., value = TRUE))

fic_tb_data_comp <- regmatches(fic_ods, regexpr("donnees_complementaires*\\.ods", fic_ods))[1] %>% 
  paste0(repertoire, "/", .)
rm(fic_ods, repertoire)

code_reg <- "52"
ter <- list_dep_in_reg(code_reg) %>%
  c(., "reg") 
noms_ter <- nom_zone(type_zone = "Départements", code_zone = list_dep_in_reg(code_reg)) %>%
  c(., nom_zone(type_zone = "Régions", code_zone = code_reg)) %>% 
  tibble(ter, nom_ter = .)
```


# Datasets

## Inventaires installations externes

### Compil TEO et ses croisements S3IC
```{r dataset}
# Exports données complémentaires pour la compilation des réponses (dites le nous une fois depuis bilan 2022)-----

compil_teo <- importer_data(table = params$compil_teo, schema = "mecc_bilans_metha", db = "production", user = params$sgbd_util)

# onglet S3IC - liste des installations en fonctionnement 
onglet_s3ic <- compil_teo %>% 
  filter(statut_agrege != "Projet") %>% 
  select(id_aile = id_table, nom_compil_teo = nom_structure_compil, statut_agrege, 
         typologie = typologie_compil,	id_compil,	s3ic = id_GUN,
         mwh_primaire = mwh_primaire_aile,	Total_tonnage = tep_aile) %>% 
  st_drop_geometry()
```

### Référentiel AILE 
Le référentiel des installations AILE à bâtir contient les évolutions des identifiants AILE et la typologie des installations, détaillé et groupée

```{r ref_aile 1}
tb_psg_id_aile <- read_ods(fic_tb_data_comp, sheet = "evol_id_aile", col_types = NA) %>% 
  replace_na(list(id_aile_old = "")) %>% 
  # on complète avec l'ensemble des installations
  full_join(onglet_s3ic %>% select(id_aile) %>% st_drop_geometry(), by = "id_aile") %>% 
  mutate(id_aile_new = id_aile) %>% 
  distinct() %>% 
  separate(id_aile_old, into = c("id_aile_old_1", "id_aile_old_2"), fill = "right") %>% 
  pivot_longer(cols = c("id_aile_old_1", "id_aile_old_2", "id_aile_new"), 
               values_to = "id_aile_old") %>% 
  filter(!is.na(id_aile_old)) %>% 
  select(-name) %>% 
  distinct() %>% 
  left_join(select(onglet_s3ic, id_aile, nom_compil_teo, typo_maj = typologie), by = "id_aile")

# verif_appariement
nrow(filter(tb_psg_id_aile, is.na(typo_maj))) == 0
```

```{r ref_aile typo}
typo_aile <- read_ods(fic_tb_data_comp, sheet = "ref_type_install", 
                              col_types = "ff")
ref_aile <- tb_psg_id_aile %>% 
  left_join(typo_aile, by = c("typo_maj" = "typo_aile")) %>% 
  select(id_aile, id_aile_old, nom_inst = nom_compil_teo, typo, typo_maj)

# verif_appariement
nrow(filter(ref_aile, is.na(typo))) == 0

poster_data(ref_aile, table = paste0("referentiel_aile_", params$campagne), 
            schema = "mecc_bilans_metha", db = "production",
            post_row_name = FALSE, pk = c("id_aile", "id_aile_old"), overwrite = TRUE, droits_schema = TRUE)
rm(tb_psg_id_aile, typo_aile)
```


```{r fonction maj id_aile, warning=FALSE}

maj_id_aile <- function(table, champ = "id_aile", garder_typo = FALSE, ajouter_nom = FALSE) {
  var_suppr <- c("id_aile_old", "typo")
  if(!garder_typo) {var_suppr <- c(var_suppr, "typo_maj")}
  if(!ajouter_nom) {var_suppr <- c(var_suppr, "nom_inst")}
  right_join(ref_aile, table, by = c("id_aile_old" = champ)) %>% 
    select(-all_of(var_suppr)) %>% 
    distinct()
}

```


```{r fonction typo aile regroupee}
regroupe_typo_aile <- function(table, champ = "typo_aile"){
  select(ref_aile, contains("typo")) %>% 
    distinct() %>% 
    right_join(table, by = c("typo_maj" = champ)) %>% 
    select(-typo_maj) %>% 
    rename(typo_aile = typo) %>% 
    distinct()
}

```  

### Registres

```{r dataset registres}
# Onglet registre électricité
onglet_reg_elec <- compil_teo %>% 
  filter(!is.na(id_rte)) %>% 
  select(id_aile,	id_RTE = id_rte) %>% 
  st_drop_geometry()

# ajout registre électricité SGBD : données au 31/12, plus récentes que celles figurant  la compil TEO
table_elec <- paste0("registre_inst_electr_", params$campagne, "_r52")
registre_elec_0 <- importer_data(table = table_elec, schema = "mecc_electricite", db = "production")
registre_elec <- registre_elec_0 %>%
  st_drop_geometry() %>%
  select(id_RTE = id_inst, nom_rte = nominstallation, date_mes_RTE = date_inst, MW_elec_RTE = puiss_mw, 
         prod_elec_MWh_an_registre = prod_mwh_an)

tb_passage_aile_registre <- left_join(onglet_reg_elec, registre_elec) %>% 
  select(id_aile,	id_RTE, nom_rte,	MW_elec_RTE, prod_elec_MWh_an_registre, date_mes_RTE) %>%
  mutate(id_aile = as.character(id_aile)) %>%
  filter(!is.na(id_aile)) %>% 
  maj_id_aile("id_aile", garder_typo = FALSE)


rm(onglet_reg_elec, table_elec, registre_elec_0, registre_elec)

# Onglet registre biométhane

# dernier registre biométhane disp dans le SGBD, données injection millésime N disponibles en août N+1
con <- connect_to_db(db = "production")
table_bioch4 <- datalibaba::list_tables(con = con, schema = "mecc_gaz", db = "production") %>% 
  grep("^registre_biogaz_...._r52$", ., value = TRUE) %>% 
  sort(x = ., decreasing = TRUE) %>% 
  .[1]
DBI::dbDisconnect(con)
registre_bioch4 <- importer_data(table = table_bioch4, schema = "mecc_gaz", db = "production") %>% 
  st_drop_geometry()

onglet_reg_ch4 <- compil_teo %>% 
  st_drop_geometry() %>% 
  mutate(id_aile = as.character(id_aile), id_unique_bioch4_registre = as.character(id_unique_bioch4_registre)) %>% 
  filter(!is.na(id_unique_bioch4_registre)) %>% 
  left_join(registre_bioch4, by = c("id_unique_bioch4_registre" = "id_unique_projet")) %>% 
  select(id_aile, id_unique_bioch4_registre, nom_du_projet, typo_registre = type, 
         date_mes_registre = date_de_mes, capa_prod_gwh_an_registre = capacite_de_production_gwh_an,
         annee, quantite_annuelle_injectee_en_gwh) %>% 
  pivot_wider(names_from = "annee", values_from = "quantite_annuelle_injectee_en_gwh", names_glue = "{.value}_{annee}") %>% 
  maj_id_aile("id_aile", garder_typo = FALSE)

rm(table_bioch4, registre_bioch4, con)

writexl::write_xlsx(x = list(reg_elec = tb_passage_aile_registre, 
                             rg_ch4 = onglet_reg_ch4, 
                             s3ic = onglet_s3ic), 
                    path = paste0("compilation/tmp/aide_donnees_complementaires_", params$campagne, ".xlsx"))
```


## Nomenclatures, objectifs et gisements intrants (issus intrants_valo)
```{r typo intrants}
# chargement de la typologie des intrants
typo_dechets <- read_ods(fic_tb_data_comp, sheet = "intrants_typo") %>% 
  # le double espace entre disparaît à la lecture du ficher ?!?
  mutate(Categorie_intrants = gsub("grasses (avec produits ", "grasses (avec  produits ", Categorie_intrants, fixed = TRUE)) %>% 
  mutate(groupe = fct_inorder(groupe), domaine_intrants = fct_inorder(domaine_intrants) ) %>% 
  bind_rows(tibble(ordre_famille = 10, domaine_intrants = "Autres",
                   famille_intrants_court = "Biodéchets", Famille_intrants = "Biodéchets",
                   Categorie_intrants = "Autres – préciser la nature", cat_intr_court = "Autres biodéchets",
                   num_ordre_cat = 58.9999, famille_srb = "G", groupe = "Autres",
                   type_gismnt_solagro = "Déchets organiques fermentescibles"))

poster_data(typo_dechets, table = paste0("referentiel_intrants_", params$campagne),
            schema = "mecc_bilans_metha", db = "production",
            post_row_name = FALSE, pk = "num_ordre_cat", overwrite = TRUE, droits_schema = TRUE)

# TODO : Attention typo_dechets à reprendre pour ajouter le autre biodéchets de 2019, et mesurer les incidences (typo dechets ensuite à supprimer de la sauvegarde du script de redressement des réponses)
# typo_dechets <- typo_dechets %>% 
#   # Ajout catégorie 2019 'autres bio déchets' 
#   bind_rows(tibble(ordre_famille = 10, domaine_intrants = "Autres",
#                    famille_intrants_court = "Biodéchets", Famille_intrants = "Biodéchets", 
#                    Categorie_intrants = "Autres – préciser la nature", cat_intr_court = "Autres biodéchets",
#                    num_ordre_cat = 58.9999, famille_srb = "G", groupe = "Autres",
#                    type_gismnt_solagro = "Déchets organiques fermentescibles"))
#
# TODO : supprimer groupe et le remplacer par domaine
    
```


### Saisies libellés, gisements et objectifs 2030 SRB 

```{r}
obj_2030 <- tribble(
  ~lib_fam_srb, ~obj_2030_tonnes, ~famille_srb, ~gisement_total,
 "Effluents d'élevage" , 	5310000 , "A" ,   22000000 ,
 "Ensilage de cultures intermédiaires"  , 1000000 , "B"  ,   3320000 ,
 "Résidus de cultures dont issus de silos"  , 145000 , "C"  ,   5323000 ,  
 "Déchets verts"  , 35000 , "D" ,   695000 ,
 "Industries agro-alimentaires (IAA) + Abattoirs" , 270000 , "E"  ,     900000 ,
 "Assainissement" , 365000 , "F" ,      990000 ,
 "Déchets organiques fermentescibles" , 70000 , "G"   ,  215000 ,  
 "Ensilage de cultures principales" , 0 , "Z", 0
 )

# vérif appariement typo_dechet, TRUE attendu
typo_dechets %>% 
  left_join(obj_2030, by = "famille_srb") %>% 
  filter(is.na(lib_fam_srb)) %>% 
  nrow() == 0 
  
poster_data(obj_2030, table = paste0("obj_2030_srb", params$campagne),
            schema = "mecc_bilans_metha", db = "production",
            post_row_name = FALSE, pk = "famille_srb", overwrite = TRUE, droits_schema = TRUE)
```

### Libellés et gisements étude Solagro/Téo 2024

```{r}
gismts_solagro <- read_ods(fic_tb_data_comp, sheet = "gismt_teo_solagro")

# vérif appariement typo_dechet, TRUE attendu
typo_dechets %>% 
  left_join(gismts_solagro, by = "type_gismnt_solagro") %>% 
  filter(is.na(ordre_solagro)) %>% 
  nrow() == 0 


poster_data(gismts_solagro, table = paste0("gisement_solagro", params$campagne),
            schema = "mecc_bilans_metha", db = "production",
            post_row_name = FALSE, pk = "ordre_solagro", overwrite = TRUE, droits_schema = TRUE)
```


# Palettes 
  - [x] famille intrants (5e rmd)
  - [x] domaines intrants (5e rmd)
  - [x] srb (5e rmd)
  - [x] srb 2 (5e rmd, L1044)
  - [x] solagro (a créer)
  - [x] pal carto 1 (5e rmd, L384)
  - [x] pal carto 2 (5e rmd, L440)
  - [x] pal-type_valo (5e rmd, ?)
  - [x] pal_typo_groupee (5e Rmd, L520)
  
## Palettes intrants

### fonction de visualisation d'une palette
```{r viz_palette}
viz_pal <- function(vec_pal) {
  barplot(rep(1, length(vec_pal)), names.arg = str_wrap(names(vec_pal), 3),
        col = vec_pal, border = NA, axes = FALSE, horiz = FALSE, cex.names = 0.6)
}

```


### Nomenclature famille
  
```{r palettes famille intrants, fig.height=2, fig.width=12}
# palette et familles
palette_famille_intrants <- tribble(
  ~famille_intrants_court, ~nom_couleur, 
  "Effluents d’élevage", "a1", 
  "CIVE",  "c2", 
  "Cultures principales", "d1", 
  "Résidus végétaux des exploitations agricoles",  "b2", 
  "Déchets végétaux IAA",  "b4", 
  "Déchets verts",  "b0", 
  "Déchets STEP urbaine",  "p1", 
  "Déchets animaux IAA",  "o0", 
  "Déchets animaux abattoirs",  "p2", 
  "Biodéchets", "g2", 
  "Glycérine",  "r3"
) %>% 
  mutate(val_couleur = gouv_colors(nom_couleur))

col_fam <- typo_dechets %>% 
  arrange(ordre_famille) %>% 
  select(famille_intrants_court, Famille_intrants) %>% 
  distinct() %>% 
  left_join(palette_famille_intrants, by = "famille_intrants_court")

# Vérif appariement palette famille (TRUE attendu)
nrow(filter(col_fam, is.na(nom_couleur))) == 0

vec_col_famille <- col_fam$val_couleur %>% setNames(col_fam$Famille_intrants)
vec_col_famille_court <- col_fam$val_couleur %>% setNames(col_fam$famille_intrants_court)

# Visualisation de la palette
viz_pal(vec_col_famille)
```

### Nomenclature SRB


```{r palettes srb, fig.height=2, fig.width=12}
palette_srb <- select(typo_dechets, famille_intrants_court, famille_srb) %>% 
  distinct() %>% 
  left_join(obj_2030 %>% select(contains("_srb")), by = "famille_srb") %>% 
  left_join(palette_famille_intrants, by = "famille_intrants_court") %>% 
  # on garde la couleur des 'Déchets animaux IAA' pour les IAA
  filter(!(famille_intrants_court %in% c("Déchets végétaux IAA", "Déchets animaux abattoirs", "Glycérine"))) %>% 
  arrange(famille_srb) %>% 
  mutate(lib_fam_srb = fct_inorder(lib_fam_srb)) %>% 
  select(contains("srb"), contains("couleur")) %>% 
  mutate(lib_fam_srb_2 = gsub("Ensilage de c", "C", lib_fam_srb))

# un sous vecteur de couleur nommées
vec_col_srb <- palette_srb$val_couleur[c(1:8)] %>% setNames(palette_srb$lib_fam_srb[c(1:8)])

vec_col_srb_2 <- palette_srb$val_couleur[c(1:8)] %>% 
  setNames(palette_srb$lib_fam_srb_2[c(1:8)])


# Visualisation de la palette
viz_pal(vec_col_srb_2)
  

```


### Nomenclature à 4 postes

```{r palettes groupes intrants, fig.height=2, fig.width=12}
col_group <- col_fam %>% 
  left_join(select(typo_dechets, famille_intrants_court, groupe), 
            by = "famille_intrants_court") %>% 
  filter(!(nom_couleur %in% c("b4", "c2", "d1", "p1"))) %>% 
  group_by(groupe) %>% 
  slice(1) %>% 
  ungroup()

vec_col_group <- col_group$val_couleur %>% setNames(col_group$groupe)

# Visualisation de la palette
viz_pal(vec_col_group)
rm(col_group, col_fam)
```

### Nomenclature gisement solagro

```{r palettes solagro, fig.height=2, fig.width=12} 
palette_solagro <- gismts_solagro %>% 
  select(type_gismnt_solagro) %>% 
  left_join(typo_dechets %>% select(famille_intrants_court, type_gismnt_solagro),
            by = "type_gismnt_solagro") %>% 
  distinct() %>% 
  filter(type_gismnt_solagro != "Hors gisements étude TEO Solagro") %>% 
  left_join(palette_famille_intrants, by = "famille_intrants_court") %>% 
  filter(!grepl("abattoir|végétaux IAA|Glycérine", famille_intrants_court)) %>% 
  select(type_gismnt_solagro, contains("couleur"))
  
vec_col_solagro <- palette_solagro$val_couleur %>% 
  setNames(palette_solagro$type_gismnt_solagro)

viz_pal(vec_col_solagro)
```


```{r palettes solagro-srb, fig.height=2, fig.width=12} 
palette_solagro_srb <- palette_srb %>% 
  filter(famille_srb != "Z")

vec_col_solagro_srb <- palette_solagro_srb$val_couleur %>% 
  setNames(palette_solagro_srb$lib_fam_srb_2)

viz_pal(vec_col_solagro_srb)

rm(palette_solagro, palette_solagro_srb)

```

## Palettes type d'installations

```{r palettes type valo, fig.height=2, fig.width=12} 
pal_typ_valo <- c("Injection" = gouv_colors("o1")[[1]], "Cogénération" = gouv_colors("f1")[[1]], "Mixte" = gouv_colors("c1")[[1]])

# # Visualisation de la palette
viz_pal(pal_typ_valo)
```


```{r palettes carto1, fig.height=2, fig.width=12} 
pal_carto_1 <- c(gouv_colors("m1")[[1]], gouv_colors("f1")[[1]], gouv_colors("c1")[[1]], gouv_colors("o1")[[1]]) %>% 
  setNames(c("Chaudière", "Cogénération", "Cogénération et injection", "Injection"))
viz_pal(pal_carto_1)
```


```{r palettes carto2, fig.height=2, fig.width=12} 
types_inst <- ref_aile %>%
  select(typo_maj, typo) %>% 
  unique() %>% 
  arrange(typo)

pal_carto_2 <- c(gouv_colors("b1")[[1]], gouv_colors("d2")[[1]], gouv_colors("r2")[[1]], 
                 gouv_colors("p2")[[1]], gouv_colors("q1")[[1]]) %>% 
  setNames(levels(types_inst$typo))
viz_pal(pal_carto_2)
rm(types_inst)
```


```{r palettes cat installation, fig.height=2, fig.width=12} 
pal_typo_groupee <- c(gouv_colors("b1")[[1]], gouv_colors("d2")[[1]], gouv_colors("r2")[[1]], gouv_colors("p2")[[1]]) %>% 
  setNames(c(names(pal_carto_2)[1:3],  "Autre"))
viz_pal(pal_typo_groupee)
```


  
#  Fonctions
  - [x] mise à jour id_aile (intro 4e rmd) --> dans § ref AILE
  - [x] regroupe typo AILE (L181 5e rmd) --> dans § ref AILE
  - [x] ligne total (intro 5e rmd)
  - [x] colonne total (intro 5e Rmd)
  - [x] ordonne intrants (5e Rmd, L152)
  

```{r}
ligne_tot <- function(df, i, col_gv = "h4"){
  row_spec(df, row = i, bold = TRUE, background = gouv_colors(col_gv))
  }

col_tot <- function(df, j, col_gv = "h4"){
  column_spec(df, column = j, bold = TRUE) #, background = gouv_colors(col_gv)
}
```



```{r ordre intrants}
# ordre des familles
famille_ordre <- palette_famille_intrants %>% 
  mutate(ordre = row_number()) %>% 
  left_join(typo_dechets, by = "famille_intrants_court") %>% 
  select(ordre, Famille_intrants, famille_intrants_court) %>% 
  distinct() %>% 
  arrange(ordre)


## Une fonction pour ordonner les tables de données intrants selon l'ordre souhaité pour les familles intrants
ordonne_intrant <- function(table, champ = "Famille_intrants"){
  
  if(grepl("cat", tolower(champ))){
    var_ordre <- "num_ordre_cat"
  } else if(grepl("srb", tolower(champ))){
    var_ordre <- "famille_srb"
  } else {
    var_ordre <- "ordre_famille"
  }
  
  right_join(typo_dechets %>% select(all_of(c(champ, var_ordre))) %>% distinct(), table, by = champ) %>% 
    arrange(across(all_of(var_ordre))) %>%  ## arrange(across(var_ordre))
    select(-all_of(var_ordre))
}

```


```{r sauv datamart}
rm(fic_tb_data_comp, viz_pal)
datamart <- setdiff(ls(), "params")
save(list = datamart, file = paste0("0-donnees_ref", params$campagne, ".RData"))
```


  
  
---
- [ ] Poster tous les référentiels dans le SGBD  
- [ ] Nettoyer et gérer les incidences dans les autres Rmd


