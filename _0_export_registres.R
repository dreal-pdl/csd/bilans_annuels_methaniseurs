library(tidyverse)
library(datalibaba)
library(sf)

# parametres
mil <- 2023
reg = "52"

# registre biométhane
table_bioch4 <- paste0("registre_biogaz_", mil, "_r", reg)
registre_bioch4 <- importer_data(table = table_bioch4, schema = "mecc_gaz", db = "production")


# registre électricité
table_elec <- paste0("registre_inst_electr_", mil, "_r", reg)
registre_elec_0 <- importer_data(table = table_elec, schema = "mecc_electricite", db = "production")
registre_elec <- registre_elec_0 %>%
  st_drop_geometry() %>%
  filter(code_typo == "metha")

writexl::write_xlsx(x = list(coge_registre_ODRE = registre_elec, inje_registre_ODRE = registre_bioch4),
                    path = paste0("compilation/", Sys.Date(), "_export_registres_", mil, ".xlsx"))
