# Les digestats {#digestats}

```{r}
save.image("RDatas_chap/03_9_intrants.RData")

# names(digestats) # créé au niveau de index.rmd 
digestat$surf <- sum(digestats$SAU_epandage_ha, na.rm = TRUE)
ha_sau_fertil_org <- 90182/0.095

t_epandues_moins_50km <- digestats %>% 
  summarise(
    # nb inst dont 100% de l'épandage est dans un rayon de 50 km et dont le tonnage épandus et connus
    nb_inst_excl = sum(!Dgt_exporte_plus50km_Oui.Non  & !is.na(Qte_digestat_produit_tMB), na.rm = TRUE), 
    # nb inst dont la distance d'épandage et le tonnage epandu sont connus :
    nb_inst_all = sum(!is.na(Dgt_exporte_plus50km_Oui.Non) & !is.na(Qte_digestat_produit_tMB), na.rm = TRUE), 
    tonnage_excl = sum(Qte_digestat_produit_tMB * !Dgt_exporte_plus50km_Oui.Non, na.rm = TRUE), 
    tonnage_all = sum(Qte_digestat_produit_tMB * Part_dgt_moins50km, na.rm = TRUE),
    poids_excl = format_fr_pct(tonnage_excl/digestat$tonnes*100, 0),
    poids_all = format_fr_pct(tonnage_all/digestat$tonnes*100, 0)) %>% 
  as.list()
  
```


> À RETENIR :<br>  
Près de `r digestat$millions_tonnes` millions de tonnes de digestats ont été déclarées (`r format_fr_nb(digestat$tonnes, 0)` t, échantillon : `r digestat$nb_inst` installations).<br>  
La surface déclarée d'épandage des digestats, `r digestat$ha_epandage` ha en `r params$campagne`, représente près de `r digestat$poids_sau_epandage` de la SAU valorisée par les agriculteurs et environ `r format_fr_pct(digestat$surf/ha_sau_fertil_org*100, 0)` des surfaces qui reçoivent un fertilisant organique (déjections animales et/ou autres matières organiques), sachant qu'environ 1,2 millions d'ha reçoivent un fertilisant minéral en 2020 (source : DRAAF - Agreste RA 2020).<br>  
`r t_epandues_moins_50km$poids_excl` des tonnages de digestats sont déclarés épandus exclusivement à moins de 50 km, par `r t_epandues_moins_50km$nb_inst_excl` installations (échantillon : `r t_epandues_moins_50km$nb_inst_all`). Pour les `r t_epandues_moins_50km$nb_inst_all - t_epandues_moins_50km$nb_inst_excl` autres installations, une partie du digestat seulement est valorisée à plus de 50 km, ce qui porte à plus de `r t_epandues_moins_50km$poids_all` la proportion de tonnages de digestats épandus dans un rayon de 50 km autour de l'installation. 

D'après les déclarations de xx installations (xxxxxxxxxx tonnes de digestats) : 

- 39 % des tonnages font exclusivement l'objet d'une séparation de phase (xx installations)  
- 24 % des tonnages sont exclusivement épandus bruts (xx installations) 
- 1% des tonnages sont exclusivement traités par compostage (x installation)  
- 2 % des tonnages sont exclusivement épandus selon les critères du cahier des charges DIG 	(x installations)   
- 35 % des tonnages font l'objet de plusieurs modes d'épandage (séparation de phase, 	épandus brut, traités par compostage, épandus selon les critères du cahier des charges 	DIG) (xx installations).   
