## Origine géographique de l'approvisionnement {#approv8}

```{r}
save.image("RDatas_chap/03_7_intrants.RData")
```


```{r origine geo}
# * vérifier que le tonnage provenant de + de 50 km ("dont_tonnage_plus_50km") < tonnage_intrant_exterieur.
anomalies_50km_tonnage <- filter(intrants_redresses, !is.na(dont_tonnage_plus_50km), dont_tonnage_plus_50km > tonnage_intrant_exterieur)
# * `dist_appro_km_max` >= 50 km si `dont_tonnage_plus_50km` renseigné
anomalies_50km_dist <- filter(intrants_redresses, !is.na(dont_tonnage_plus_50km), dont_tonnage_plus_50km!= 0, dist_appro_km_max < 50) %>% 
  select(id_aile, nom = entreprise_raison_sociale, cat_intrant, tonnage_intrant_exterieur, dont_tonnage_plus_50km, dist_appro_km_max) %>% 
  mutate(cat_intrant = substr(cat_intrant, 0, 50))
# * `dist_appro_km_max` renseigné si `tonnage_intrant_exterieur` renseigné.
anomalies_saisie_dist <- filter(intrants_redresses, is.na(dist_appro_km_max), tonnage_intrant_exterieur > 0) %>% 
  select(id_aile, nom = entreprise_raison_sociale, cat_intrant, tonnage_intrant_exterieur, dont_tonnage_plus_50km, dist_appro_km_max) %>% 
  mutate(cat_intrant = substr(cat_intrant, 0, 50))


geo_tonnages <- intrants_redresses %>% 
  group_by(id_aile) %>% 
  summarise(
    appro_externe = any(tonnage_intrant_exterieur > 0, FALSE, na.rm = TRUE),
    dist_appro_km_max = weighted.mean(dist_appro_km_max, tonnage_intrant_exterieur, na.rm = TRUE),
    tonnage_intrant_exterieur = sum(tonnage_intrant_exterieur, na.rm = TRUE),
    tonnage_plus_50km = sum(dont_tonnage_plus_50km, na.rm = TRUE), 
    dist_appro_km_max_ok = all(appro_externe, !is.na(dist_appro_km_max)),
    tonnage_plus_50km_ok = all(appro_externe, tonnage_plus_50km !=0),
    .groups = "drop"
  )

geo_tonnages_sum <- geo_tonnages %>% 
  summarise(nb_inst_appro_ext = sum(appro_externe),
            dist_appro_km_max_ok = sum(dist_appro_km_max_ok),
            dist_appro_km_max = weighted.mean(dist_appro_km_max, tonnage_intrant_exterieur, na.rm = TRUE) %>% format_fr_nb(0),
            tonnage_plus_50km_pct = (sum(tonnage_plus_50km) / vol_reg * 100 )%>% format_fr_pct(0),
            tonnage_plus_50km = sum(tonnage_plus_50km) %>% format_fr_nb(0),
            tonnage_plus_50km_ok = sum(tonnage_plus_50km_ok)) %>% 
  as.list()

geo_intrants <- compil_reponses5 %>% 
  filter(typo_aile != "ISDND") %>% 
  mutate(autocons = tonnage_intrant_prod_en_propre == ration_t,
         part_autocons = coalesce(tonnage_intrant_prod_en_propre, 0) / ration_t * 100) %>%  # View()
  summarise(full_autocons_nb = sum(autocons, na.rm = TRUE),
            full_autocons_part = sum(autocons, na.rm = TRUE) / n_distinct(id_aile) * 100,
            full_autocons_tonnage = sum(autocons * ration_t, na.rm = TRUE) / vol_reg * 100,
            full_ext_nb = sum(part_autocons == 0, na.rm = TRUE),
            full_ext_part = sum(part_autocons == 0, na.rm = TRUE) / n_distinct(id_aile) * 100,
            full_ext_tonnage = sum((part_autocons == 0) * ration_t, na.rm = TRUE) / vol_reg * 100,
            autocons_75pct_nb = sum(part_autocons >= 75),
            autocons_75pct_part = sum(part_autocons >= 75) / n_distinct(id_aile) * 100,
            autocons_75pct_tonnage = sum((part_autocons >= 75) * ration_t, na.rm = TRUE) / vol_reg * 100,
            autocons_80pct_nb = sum(part_autocons >= 80),
            autocons_80pct_part = sum(part_autocons >= 80) / n_distinct(id_aile) * 100,
            autocons_80pct_tonnage = sum((part_autocons >= 80) * ration_t, na.rm = TRUE) / vol_reg * 100,
            nb_rep_intrants = n_distinct(id_aile),
            intrant_autocons_part = sum(tonnage_intrant_prod_en_propre, na.rm = TRUE) / sum(ration_t, na.rm = TRUE) * 100,
            taux_autoconso_moy = mean(part_autocons)) %>% 
  mutate(across(c(ends_with("_part"), ends_with("_tonnage"), taux_autoconso_moy), ~format_fr_pct(.x, 0))) %>% 
  as.list()
 

```

> A RETENIR :    
>- `r geo_intrants$intrant_autocons_part` % des intrants sont consommés sur leur lieu de production.   


En moyenne, les `r nb_rep_intrants` installations qui ont détaillé leurs intrants `r params$campagne`, s'auto-approvisionnent à hauteur de `r geo_intrants$taux_autoconso_moy` de leur ration. 
De manière globale, les intrants produits en propre par les installations, qui générant très peu de déplacements, représentent `r geo_intrants$intrant_autocons_part` du volume intrants total.  

Sur les `r nb_rep_intrants` déclarations intrants de `r params$campagne`, on dénombre `r geo_intrants$full_autocons_nb` installations dont les intrants proviennent exclusivement de leur installation (`r geo_intrants$full_autocons_part` des installations, représentant `r geo_intrants$full_autocons_tonnage` du tonnage total). 
A contrario, `r geo_intrants$full_ext_nb` installations, principalement des installations de type 'Centralisé mono-acteur', s'approvisionnent exclusivement à l'extérieur (`r geo_intrants$full_ext_part` des installations, représentant `r geo_intrants$full_ext_tonnage` du tonnage total). 

Parmi les `r geo_tonnages_sum$nb_inst_appro_ext` installations s'approvisionnant à l'extérieur, seules `r geo_tonnages_sum$dist_appro_km_max_ok` installations ont renseigné leur distance d'approvisionnement, rendant l'exploitation statistique de cette informations très hasardeuse. 
Cette précaution prise, pour ces `r geo_tonnages_sum$dist_appro_km_max_ok` installations, la distance maximale d'approvisionnement vaut en moyenne `r geo_tonnages_sum$dist_appro_km_max` km (moyenne pondérée par le tonnage des intrants).

`r geo_tonnages_sum$tonnage_plus_50km_ok` installations indiquent compter dans leur approvisionnement, des intrants originaires de plus de 50 km de l'installation.
Ces intrants ayant parcouru plus de 50 km représentent `r geo_tonnages_sum$tonnage_plus_50km_pct` des intrants (`r geo_tonnages_sum$tonnage_plus_50km` tonnes).

```{r tab origine geo}

geo_intrants <- intrants_redresses %>% 
  group_by(Famille_intrants) %>% 
  summarise(
    tonnage_total = sum(tonnage_total, na.rm = TRUE),
    tonnage_intrant_prod_en_propre = sum(tonnage_intrant_prod_en_propre, na.rm = TRUE),
    part_tonnages_internes = tonnage_intrant_prod_en_propre / tonnage_total,
    tonnage_intrant_exterieur = sum(tonnage_intrant_exterieur, na.rm = TRUE),
    part_tonnages_externes = tonnage_intrant_exterieur / tonnage_total,
    tonnage_plus_50km = sum(dont_tonnage_plus_50km, na.rm = TRUE), 
    part_plus_50km = tonnage_plus_50km / tonnage_total,
    .groups = "drop"
  ) %>% 
  ordonne_intrant() %>% 
  select(-tonnage_plus_50km)

geo_intrants %>% 
  mutate(across(starts_with("part_"), ~format_fr_pct(x = .x*100, dec = 1))) %>% 
  knitr::kable(caption = "Répartition de l'approvionnement selon l'origine géographique", digits = 0, label = NA, 
               align = c("lrrrrrr"),
               format.args = list(decimal.mark = ",", big.mark = "\u202f", scientific = FALSE), format = "html",
               col.names	= c("Famille d'intrants", "Tonnes", "Tonnes", "%", "Tonnes", "%", "Dont % provenant de plus de 50 km")) %>% 
    add_header_above(c(" " = 1, "Approvisionnement total déclaré" = 1, 
                       "Approvisionnement produit en propre déclaré (tonnes et % de l’approvisionnement total)" = 2,
                     "Approvisionnement produit en extérieur déclaré (tonnes et % de l’approvisionnement total)" = 3)) %>%
    kable_styling(full_width = TRUE, font_size = 11.5) %>% 
    footnote("", fixed_small_size = TRUE, general_title = filigrane)

```

